// For each (target) project, create a corresponding service account
// and key file inside the (source) token rotation GCP project.
// That key file will then be placed into the {project}-token-rotation bucket
// for each target project. This can be used for impersonation and token
// rotation tasks.
// ("gl_env_name:token-rotation-management" for more information)

resource "google_service_account" "token-rotation-keyfile-owner" {
  count = var.token_rotation_project_id != null ? 1 : 0

  account_id  = format("token-rotation-%s", google_project.project.number)
  project     = var.token_rotation_project_id
  description = "Used to generate credentials for the gl_env_name:token-rotation-management project"
}

resource "google_project_iam_member" "token-rotation-keyfile-owner-browser" {
  count = var.token_rotation_project_id != null ? 1 : 0

  project = var.token_rotation_project_id
  role    = "roles/browser"
  member  = google_service_account.token-rotation-keyfile-owner[0].member
}

resource "google_service_account_key" "service_account_key" {
  count = var.token_rotation_project_id != null ? 1 : 0

  service_account_id = google_service_account.token-rotation-keyfile-owner[0].id
  public_key_type    = "TYPE_X509_PEM_FILE"
}

resource "google_storage_bucket_object" "service_account_key_file" {
  count = var.token_rotation_project_id != null ? 1 : 0

  name    = "service-account-key.json"
  bucket  = google_storage_bucket.token_bucket[0].name
  content = base64decode(google_service_account_key.service_account_key[0].private_key)

  metadata = {
    gl_realm    = "infra-shared-services"
    gl_env_type = "test"
    gl_env_name = "token-rotation-management"
    tfmanaged   = "yes"
  }
}
