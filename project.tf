// Project Creation
resource "google_project" "project" {
  name       = local.project_name
  project_id = var.project
  folder_id  = var.project_folder

  billing_account = var.billing_account
  labels          = local.labels

  auto_create_network = var.auto_create_network
  deletion_policy     = var.project_deletion_policy
}
