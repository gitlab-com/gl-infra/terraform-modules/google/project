module "gitlab-staging-audit_config" {
  source  = "terraform-google-modules/iam/google//modules/audit_config"
  version = "8.1.0"

  project          = google_project.project.project_id
  audit_log_config = var.audit_log_config
}
