locals {
  # Project name
  project_name = var.project_name != "" ? var.project_name : var.project

  # Bucket names
  secrets_bucket_name = coalesce(var.secrets_bucket_name, local.project_name)
  token_bucket_name   = coalesce(var.token_bucket_name, local.project_name)

  # Custom roles
  custom_role_storage_viewer_id         = "storage.viewer"
  custom_role_quota_statistic_viewer_id = "quota_statistics.viewer"

  # SA roles
  terraform_default_roles = {
    readwrite = [
      "roles/owner",
    ]
    readonly = [
      "roles/iam.securityReviewer",
      "roles/viewer",
    ]
  }
  terraform_roles = {
    readonly  = setunion(local.terraform_default_roles.readonly, var.terraform.readonly.roles)
    readwrite = setunion(local.terraform_default_roles.readwrite, var.terraform.readwrite.roles)
  }
  terraform_service_account_roles = {
    readonly = {
      for sr in setproduct(var.terraform.readonly.service_account_emails, local.terraform_roles.readonly) :
      format("%s:%s", sr...) => { email = sr[0], role = sr[1] }
    }
    readwrite = {
      for sr in setproduct(var.terraform.readwrite.service_account_emails, local.terraform_roles.readwrite) :
      format("%s:%s", sr...) => { email = sr[0], role = sr[1] }
    }
  }

  # Labels
  labels = merge(
    {
      "tfmanaged" = "yes"
    },
    var.additional_labels
  )
}
