// Create storage bucket for chef secrets
resource "google_storage_bucket" "secrets" {
  project = google_project.project.project_id
  name    = format("%s-secrets", local.secrets_bucket_name)

  storage_class = "MULTI_REGIONAL"
  location      = "US"

  uniform_bucket_level_access = true
  public_access_prevention    = "enforced"

  force_destroy = true

  versioning {
    enabled = true
  }

  labels = {
    "tfmanaged" = "yes"
  }
}

// Create storage bucket for token-rotation-management
resource "google_storage_bucket" "token_bucket" {
  count = var.token_rotation_project_id != null ? 1 : 0

  project = google_project.project.project_id
  name    = format("%s-token-rotation", local.token_bucket_name)

  storage_class = "MULTI_REGIONAL"
  location      = "US"

  uniform_bucket_level_access = true
  public_access_prevention    = "enforced"

  force_destroy = true

  versioning {
    enabled = true
  }

  labels = {
    gl_realm    = "infra-shared-services"
    gl_env_type = "test"
    gl_env_name = "token-rotation-management"
    tfmanaged   = "yes"
  }
}
