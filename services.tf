// Enable Project API Services
resource "google_project_service" "api_services" {
  for_each = var.api_services

  project            = google_project.project.project_id
  service            = each.value
  disable_on_destroy = false
}
