resource "google_compute_project_metadata_item" "default" {
  project = google_project.project.project_id
  key     = "serial-port-logging-enable"
  value   = var.serial_port_logging_enable
}

resource "google_compute_project_metadata_item" "disable-endpoints" {
  project = google_project.project.project_id
  key     = "disable-legacy-endpoints"
  value   = "TRUE"
}
