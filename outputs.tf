// The project ID is important to other modules since they will only have the name
output "project_id" {
  value = google_project.project.project_id
}

output "project_number" {
  value = google_project.project.number
}

output "terraform_legacy_service_account" {
  value = var.create_legacy_service_account ? one(google_service_account.terraform_legacy) : null
}

output "terraform_legacy_service_account_roles" {
  value = var.create_legacy_service_account ? var.legacy_service_account_roles : null
}

output "gcp_token_mapping" {
  value = {
    keyfile_id   = one(google_service_account_key.service_account_key[*].id)
    project_name = var.project_name
  }
}
