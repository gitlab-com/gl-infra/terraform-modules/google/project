# GitLab.com Project Terraform Module

## What is this?

This module provisions a single GCP project. Currently we are using this to
manage projects from Terraform configurations in the
[Environments](https://ops.gitlab.net/gitlab-com/environments),
[config-mgmt](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/tree/master/environments/env-projects),
[Group Projects](https://ops.gitlab.net/gitlab-com/group-projects), and
[Services](https://ops.gitlab.net/gitlab-com/services-base) terraform repositories.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 5.23.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 5.23.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_gitlab-staging-audit_config"></a> [gitlab-staging-audit\_config](#module\_gitlab-staging-audit\_config) | terraform-google-modules/iam/google//modules/audit_config | 8.1.0 |
| <a name="module_iam-bindings"></a> [iam-bindings](#module\_iam-bindings) | terraform-google-modules/iam/google//modules/projects_iam | 8.1.0 |

## Resources

| Name | Type |
|------|------|
| [google_cloud_quotas_quota_preference.preference](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_quotas_quota_preference) | resource |
| [google_compute_project_metadata_item.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_project_metadata_item) | resource |
| [google_compute_project_metadata_item.disable-endpoints](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_project_metadata_item) | resource |
| [google_project.project](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project) | resource |
| [google_project_iam_custom_role.patch_automation_compute_viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_custom_role) | resource |
| [google_project_iam_custom_role.read_quota_statistics](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_custom_role) | resource |
| [google_project_iam_custom_role.storage-viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_custom_role) | resource |
| [google_project_iam_member.gcp_quota_exporter_iam_member](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.patch-automation-iam-member](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.terraform-readonly](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.terraform-readonly-storage-viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.terraform-readwrite](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.terraform_legacy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.token-rotation-keyfile-owner-browser](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_service.api_services](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_service) | resource |
| [google_service_account.terraform_legacy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account.token-rotation-keyfile-owner](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account_key.service_account_key](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_key) | resource |
| [google_storage_bucket.secrets](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket.token_bucket](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket_object.service_account_key_file](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_object) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_labels"></a> [additional\_labels](#input\_additional\_labels) | Additional key/value pairs to add as labels to the project | `map(string)` | `{}` | no |
| <a name="input_api_services"></a> [api\_services](#input\_api\_services) | The list of APIs to activate within the project | `set(string)` | <pre>[<br/>  "serviceusage.googleapis.com",<br/>  "cloudbilling.googleapis.com",<br/>  "compute.googleapis.com",<br/>  "container.googleapis.com",<br/>  "cloudkms.googleapis.com",<br/>  "storage-api.googleapis.com",<br/>  "dns.googleapis.com",<br/>  "policyanalyzer.googleapis.com"<br/>]</pre> | no |
| <a name="input_audit_log_config"></a> [audit\_log\_config](#input\_audit\_log\_config) | List of objects to configure audit logging. This takes the `audit_log_config` from https://registry.terraform.io/modules/terraform-google-modules/iam/google/latest/submodules/audit_config. | <pre>list(object({<br/>    service          = string<br/>    log_type         = string<br/>    exempted_members = list(string)<br/>  }))</pre> | `[]` | no |
| <a name="input_auto_create_network"></a> [auto\_create\_network](#input\_auto\_create\_network) | Controls whether the 'default' network is created on the project | `bool` | `true` | no |
| <a name="input_billing_account"></a> [billing\_account](#input\_billing\_account) | Billing account of the organization | `string` | n/a | yes |
| <a name="input_create_legacy_service_account"></a> [create\_legacy\_service\_account](#input\_create\_legacy\_service\_account) | Creates the legacy service account 'terraform' that is used on VMs | `bool` | `false` | no |
| <a name="input_iam"></a> [iam](#input\_iam) | List of additive IAM bindings for project roles. | <pre>object({<br/>    bindings = optional(map(list(string)), {})<br/>    conditional_bindings = optional(list(object({<br/>      role        = string<br/>      title       = string<br/>      description = string<br/>      expression  = string<br/>      members     = list(string)<br/>    })), [])<br/>  })</pre> | `{}` | no |
| <a name="input_legacy_service_account_roles"></a> [legacy\_service\_account\_roles](#input\_legacy\_service\_account\_roles) | List of IAM roles to which the legacy terraform service account will be assigned | `set(string)` | <pre>[<br/>  "roles/cloudtrace.agent",<br/>  "roles/cloudprofiler.agent",<br/>  "roles/compute.admin",<br/>  "roles/logging.logWriter",<br/>  "roles/monitoring.viewer",<br/>  "roles/pubsub.editor",<br/>  "roles/pubsub.publisher",<br/>  "roles/pubsub.subscriber",<br/>  "roles/iam.serviceAccountTokenCreator",<br/>  "roles/iam.serviceAccountUser"<br/>]</pre> | no |
| <a name="input_patch_automation_iam_member"></a> [patch\_automation\_iam\_member](#input\_patch\_automation\_iam\_member) | Google service accounts that should be granted access to view Compute instances. | `string` | `null` | no |
| <a name="input_project"></a> [project](#input\_project) | The project ID | `string` | n/a | yes |
| <a name="input_project_deletion_policy"></a> [project\_deletion\_policy](#input\_project\_deletion\_policy) | Deletion Policy to set on the project. Valid values are `PREVENT`, `ABANDON` and `DELETE` | `string` | `"PREVENT"` | no |
| <a name="input_project_folder"></a> [project\_folder](#input\_project\_folder) | The folder holding the project | `string` | n/a | yes |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | Override the project name, defaults to project ID | `string` | `""` | no |
| <a name="input_quota_contact_email"></a> [quota\_contact\_email](#input\_quota\_contact\_email) | Contact email used for quota increases | `string` | `"ops-contact@gitlab.com"` | no |
| <a name="input_quota_exporter_iam_members"></a> [quota\_exporter\_iam\_members](#input\_quota\_exporter\_iam\_members) | Google service accounts that should be granted access to view Quota metrics. | `set(string)` | `[]` | no |
| <a name="input_quotas"></a> [quotas](#input\_quotas) | Optionally set quota preferred values | <pre>list(object({<br/>    service         = string<br/>    quota_id        = string<br/>    region          = string<br/>    preferred_value = number<br/>  }))</pre> | `[]` | no |
| <a name="input_secrets_bucket_name"></a> [secrets\_bucket\_name](#input\_secrets\_bucket\_name) | Base string for secrets bucket name, suffixed with `-secrets`  (default: `var.project_name`) | `string` | `null` | no |
| <a name="input_serial_port_logging_enable"></a> [serial\_port\_logging\_enable](#input\_serial\_port\_logging\_enable) | Enable or disable serial port logging at the project level | `bool` | `false` | no |
| <a name="input_terraform"></a> [terraform](#input\_terraform) | Service accounts used by Terraform and the roles to bind to them | <pre>object({<br/>    enabled = optional(bool, true)<br/>    readonly = optional(object({<br/>      service_account_emails = optional(set(string), [])<br/>      roles                  = optional(set(string), [])<br/>    }), {})<br/>    readwrite = optional(object({<br/>      service_account_emails = optional(set(string), [])<br/>      roles                  = optional(set(string), [])<br/>    }), {})<br/>  })</pre> | n/a | yes |
| <a name="input_token_bucket_name"></a> [token\_bucket\_name](#input\_token\_bucket\_name) | Base string for token rotation bucket name, suffixed with `-token-rotation`  (default: `var.project_name`) | `string` | `null` | no |
| <a name="input_token_rotation_project_id"></a> [token\_rotation\_project\_id](#input\_token\_rotation\_project\_id) | Token rotation project ID | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_gcp_token_mapping"></a> [gcp\_token\_mapping](#output\_gcp\_token\_mapping) | n/a |
| <a name="output_project_id"></a> [project\_id](#output\_project\_id) | The project ID is important to other modules since they will only have the name |
| <a name="output_project_number"></a> [project\_number](#output\_project\_number) | n/a |
| <a name="output_terraform_legacy_service_account"></a> [terraform\_legacy\_service\_account](#output\_terraform\_legacy\_service\_account) | n/a |
| <a name="output_terraform_legacy_service_account_roles"></a> [terraform\_legacy\_service\_account\_roles](#output\_terraform\_legacy\_service\_account\_roles) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
