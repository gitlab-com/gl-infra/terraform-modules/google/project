// https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_quotas_quota_preference
resource "google_cloud_quotas_quota_preference" "preference" {
  for_each = { for q in var.quotas : "${replace(q.service, ".", "_")}-${q.quota_id}_${q.region}" => q }

  parent        = "projects/${var.project}"
  name          = each.key // Example: compute_googleapis_com-CPUS-per-project_us-east1
  dimensions    = { region = each.value.region }
  service       = each.value.service
  quota_id      = each.value.quota_id
  contact_email = var.quota_contact_email
  quota_config {
    preferred_value = each.value.preferred_value
  }
}
