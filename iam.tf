// Create the generic "terraform" service account that is used
// for legacy VMs.
resource "google_service_account" "terraform_legacy" {
  count = var.create_legacy_service_account ? 1 : 0

  project      = google_project.project.project_id
  account_id   = "terraform"
  display_name = "Automation Service Account"
}

resource "google_project_iam_member" "terraform_legacy" {
  for_each = var.create_legacy_service_account ? var.legacy_service_account_roles : []

  project = google_project.project.project_id
  role    = each.value
  member  = google_service_account.terraform_legacy[0].member
}

// Custom roles

resource "google_project_iam_custom_role" "storage-viewer" {
  project     = google_project.project.project_id
  role_id     = local.custom_role_storage_viewer_id
  title       = "Storage Viewer"
  description = "Grants access to view storage buckets and their metadata, excluding ACLs."
  permissions = [
    "storage.buckets.get",
    "storage.buckets.list",
  ]
}

resource "google_project_iam_custom_role" "patch_automation_compute_viewer" {
  project     = google_project.project.project_id
  count       = var.patch_automation_iam_member != null ? 1 : 0
  role_id     = "patchAutomationComputeViewer"
  title       = "Patch Automation Compute Viewer"
  description = "Patch Automation role to list compute instances"
  permissions = [
    "compute.instances.list",
    "compute.instances.get",
  ]
}

resource "google_project_iam_custom_role" "read_quota_statistics" {
  count = length(var.quota_exporter_iam_members) > 0 ? 1 : 0

  project     = google_project.project.project_id
  role_id     = local.custom_role_quota_statistic_viewer_id
  title       = "Quota Statistics Viewer"
  description = "Grants access to collect quota statistics, used by the gcp-quota-exporters."
  permissions = [
    "compute.projects.get",
    "compute.regions.list",
    "compute.instances.list",
    "serviceusage.quotas.get",
  ]
}

resource "google_project_iam_member" "gcp_quota_exporter_iam_member" {
  for_each = var.quota_exporter_iam_members

  project = google_project.project.project_id
  role    = google_project_iam_custom_role.read_quota_statistics[0].name
  member  = each.value
}

// Terraform Service Account impersonated via Vault

resource "google_project_iam_member" "terraform-readonly-storage-viewer" {
  for_each = var.terraform.enabled ? var.terraform.readonly.service_account_emails : []
  project  = google_project.project.project_id
  role     = google_project_iam_custom_role.storage-viewer.name
  member   = "serviceAccount:${each.value}"
}

resource "google_project_iam_member" "terraform-readonly" {
  for_each = var.terraform.enabled ? local.terraform_service_account_roles.readonly : {}

  project = google_project.project.project_id
  role    = each.value.role
  member  = "serviceAccount:${each.value.email}"
}

resource "google_project_iam_member" "terraform-readwrite" {
  for_each = var.terraform.enabled ? local.terraform_service_account_roles.readwrite : {}

  project = google_project.project.project_id
  role    = each.value.role
  member  = "serviceAccount:${each.value.email}"
}

resource "google_project_iam_member" "patch-automation-iam-member" {
  count = var.patch_automation_iam_member != null ? 1 : 0

  project = google_project.project.project_id
  role    = google_project_iam_custom_role.patch_automation_compute_viewer[0].name
  member  = var.patch_automation_iam_member
}

// Additional IAM bindings

module "iam-bindings" {
  source  = "terraform-google-modules/iam/google//modules/projects_iam"
  version = "8.1.0"

  projects = [google_project.project.project_id]
  mode     = "additive"

  bindings             = var.iam.bindings
  conditional_bindings = var.iam.conditional_bindings
}
