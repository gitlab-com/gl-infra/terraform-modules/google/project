//
// Required variables
//

// The billing account to assign the project to
variable "billing_account" {
  type        = string
  description = "Billing account of the organization"
}

// The project id
variable "project" {
  type        = string
  description = "The project ID"
}

// The folder containing the project
variable "project_folder" {
  type        = string
  description = "The folder holding the project"
}

//
// Optional variables
//

// The API Services to enable on the project
variable "api_services" {
  type        = set(string)
  description = "The list of APIs to activate within the project"
  default = [
    "serviceusage.googleapis.com",
    "cloudbilling.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
    "cloudkms.googleapis.com",
    "storage-api.googleapis.com",
    "dns.googleapis.com",
    "policyanalyzer.googleapis.com",
  ]
}

// Secrets bucket
variable "secrets_bucket_name" {
  type        = string
  description = "Base string for secrets bucket name, suffixed with `-secrets`  (default: `var.project_name`)"
  default     = null
}

// Token rotation bucket
variable "token_bucket_name" {
  type        = string
  description = "Base string for token rotation bucket name, suffixed with `-token-rotation`  (default: `var.project_name`)"
  default     = null
}

// Provide a custom string for project name
variable "project_name" {
  type        = string
  description = "Override the project name, defaults to project ID"
  default     = ""
}

variable "auto_create_network" {
  type        = bool
  description = "Controls whether the 'default' network is created on the project"
  default     = true
}

variable "terraform" {
  type = object({
    enabled = optional(bool, true)
    readonly = optional(object({
      service_account_emails = optional(set(string), [])
      roles                  = optional(set(string), [])
    }), {})
    readwrite = optional(object({
      service_account_emails = optional(set(string), [])
      roles                  = optional(set(string), [])
    }), {})
  })
  description = "Service accounts used by Terraform and the roles to bind to them"
}

variable "legacy_service_account_roles" {
  type        = set(string)
  description = "List of IAM roles to which the legacy terraform service account will be assigned"
  default = [
    "roles/cloudtrace.agent",
    "roles/cloudprofiler.agent",
    "roles/compute.admin",
    "roles/logging.logWriter",
    "roles/monitoring.viewer",
    "roles/pubsub.editor",
    "roles/pubsub.publisher",
    "roles/pubsub.subscriber",
    "roles/iam.serviceAccountTokenCreator",
    "roles/iam.serviceAccountUser",
  ]
}

// Whether serial port logging is enabled for all VMs by default (unless overridden per VM)
variable "serial_port_logging_enable" {
  type        = bool
  description = "Enable or disable serial port logging at the project level"
  default     = false
}

variable "additional_labels" {
  type        = map(string)
  description = "Additional key/value pairs to add as labels to the project"
  default     = {}
}

variable "create_legacy_service_account" {
  type        = bool
  description = "Creates the legacy service account 'terraform' that is used on VMs"
  default     = false
}

variable "iam" {
  type = object({
    bindings = optional(map(list(string)), {})
    conditional_bindings = optional(list(object({
      role        = string
      title       = string
      description = string
      expression  = string
      members     = list(string)
    })), [])
  })
  description = "List of additive IAM bindings for project roles."
  default     = {}
}

variable "audit_log_config" {
  type = list(object({
    service          = string
    log_type         = string
    exempted_members = list(string)
  }))
  description = "List of objects to configure audit logging. This takes the `audit_log_config` from https://registry.terraform.io/modules/terraform-google-modules/iam/google/latest/submodules/audit_config."
  default     = []
}

variable "token_rotation_project_id" {
  type        = string
  description = "Token rotation project ID"
  default     = null
}

variable "quotas" {
  type = list(object({
    service         = string
    quota_id        = string
    region          = string
    preferred_value = number
  }))
  description = "Optionally set quota preferred values"
  default     = []
}

variable "quota_contact_email" {
  type        = string
  description = "Contact email used for quota increases"
  default     = "ops-contact@gitlab.com"
}

variable "quota_exporter_iam_members" {
  type        = set(string)
  default     = []
  description = "Google service accounts that should be granted access to view Quota metrics."
}

variable "patch_automation_iam_member" {
  type        = string
  nullable    = true
  default     = null
  description = "Google service accounts that should be granted access to view Compute instances."
}

variable "project_deletion_policy" {
  type        = string
  nullable    = false
  default     = "PREVENT"
  description = "Deletion Policy to set on the project. Valid values are `PREVENT`, `ABANDON` and `DELETE`"
  validation {
    condition     = contains(["PREVENT", "ABANDON", "DELETE"], var.project_deletion_policy)
    error_message = "Allowed values for project_deletion_policy are 'PREVENT', 'ABANDON', or 'DELETE'."
  }
}
